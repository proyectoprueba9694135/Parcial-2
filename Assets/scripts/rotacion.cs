using UnityEngine;

public class rotacion : MonoBehaviour
{
   
    public float velocidadRotacion = 500f;

    void Update()
    {
       
        Rotate();
    }

    void Rotate()
    {
        
        float rotacion = Time.deltaTime * velocidadRotacion;

        
        transform.Rotate(rotacion,0f, 0f);
    }
}

