using UnityEngine;

public class respuestacorrecta : MonoBehaviour
{
    public GameObject door;
    public Material cambiarcolor;
    public GameObject objetoADestruir; 
    private int objectsOnPlatform = 0;
    private Material originalMaterial;

    void Start()
    {
        originalMaterial = GetComponent<Renderer>().material;
    }

    void Update()
    {
        if (objectsOnPlatform > 0)
        {
            door.SetActive(false);
            GetComponent<Renderer>().material = cambiarcolor;
            DestruirObjeto();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Caja"))
        {
            objectsOnPlatform++;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Caja"))
        {
            objectsOnPlatform--;
        }
    }

    void DestruirObjeto()
    {
        if (objetoADestruir != null)
        {
            Destroy(objetoADestruir);
        }
    }
}

