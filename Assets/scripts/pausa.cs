using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Necesario para reiniciar el juego
using UnityEngine.UI; // Necesario si quieres interactuar con UI en Unity

public class pausa : MonoBehaviour
{
    public GameObject objetomenupausa;
    public bool Pausa = false;

    void Start()
    {
        // Asegurarse de que el men� de pausa est� desactivado al inicio
        objetomenupausa.SetActive(false);
    }

    void Update()
    {
        // Cambiar el bot�n de pausa a "P"
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (Pausa == false)
            {
                PausarJuego();
            }
            else
            {
                ReanudarJuego();
            }
        }
    }

    public void PausarJuego()
    {
        objetomenupausa.SetActive(true); 
        Pausa = true; 
        Time.timeScale = 0; 
    }

    public void ReanudarJuego()
    {
        objetomenupausa.SetActive(false); 
        Pausa = false; 
        Time.timeScale = 1;
    }

    public void ReiniciarJuego()
    {
        Time.timeScale = 1; 
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
    }

    public void SalirDelJuego()
    {
        Application.Quit();
    }
}
