using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambiodecolor : MonoBehaviour
{
    public Material colorcambiante;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            CambiarColorPelota(other.gameObject);
            Destroy(gameObject); 
        }
    }

    void CambiarColorPelota(GameObject pelota)
    {
        Renderer rend = pelota.GetComponent<Renderer>();
        rend.material = colorcambiante;
    }
}
