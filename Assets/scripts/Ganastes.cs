using UnityEngine;
using TMPro;

public class Ganastes : MonoBehaviour
{
    public TMP_Text mensajeGanaste; 

    private bool victoriaMostrada = false;

    void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player") && !victoriaMostrada)
        {
            MostrarMensajeGanaste();
            victoriaMostrada = true;
        }
    }

    void MostrarMensajeGanaste()
    {
        
        mensajeGanaste.text = "¡Ganaste!";
    }
}
