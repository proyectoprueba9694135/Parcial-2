using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class agarrarcaja : MonoBehaviour
{
    public GameObject puntodemano; 
    public float pickupSmoothness = 5f;  

    private GameObject picketobject = null;
    private Collider playerCollider; 

    void Start()
    {
        playerCollider = GetComponentInParent<Collider>();
    }

    void Update()
    {
        if (picketobject != null)
        {
           
            picketobject.transform.position = Vector3.Lerp(picketobject.transform.position, puntodemano.transform.position, Time.deltaTime * pickupSmoothness);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Caja"))
        {
            if (Input.GetKey("e") && picketobject == null)
            {
                
                Physics.IgnoreCollision(playerCollider, other, true);

                Rigidbody rb = other.GetComponent<Rigidbody>();
                rb.useGravity = false;
                rb.isKinematic = true;

                picketobject = other.gameObject;
                picketobject.transform.SetParent(puntodemano.transform);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (picketobject != null && other.gameObject == picketobject)
        {
            
            Physics.IgnoreCollision(playerCollider, other, false);

            Rigidbody rb = picketobject.GetComponent<Rigidbody>();
            rb.useGravity = true;
            rb.isKinematic = false;

            picketobject.transform.SetParent(null);
            picketobject = null;
        }
    }
}
