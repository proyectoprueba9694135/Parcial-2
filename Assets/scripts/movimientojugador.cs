
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientojugador : MonoBehaviour
{
    public float velocidad = 5f;
    private Rigidbody rb;
    private bool puedeMoverse = true;

    void Start()
    {
        // Obt�n el componente Rigidbody al inicio
        rb = GetComponent<Rigidbody>();

        // Congela la rotaci�n en todos los ejes para evitar giros inesperados
        rb.freezeRotation = true;
    }

    void Update()
    {
        if (puedeMoverse)
        {
            float movimientoHorizontal = Input.GetAxis("Horizontal");
            float movimientoVertical = Input.GetAxis("Vertical");

            Vector3 movimiento = new Vector3(movimientoHorizontal, 0f, movimientoVertical).normalized * velocidad * Time.deltaTime;

            // Calcula el movimiento relativo a la c�mara
            movimiento = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y, 0) * movimiento;

            // Aplica el movimiento usando Rigidbody
            rb.MovePosition(transform.position + movimiento);
        }
    }

    public void DesactivarMovimiento()
    {
        puedeMoverse = false;
    }
}