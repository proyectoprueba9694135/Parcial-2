using UnityEngine;

public class saltojugador : MonoBehaviour
{
    public float velocidad = 5f;
    public float fuerzaSalto = 10f;
    private int saltosRestantes = 2;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (EstaEnElSuelo())
        {
            saltosRestantes = 2; // Restablecer los saltos restantes cuando toca el suelo
        }

        if (Input.GetKeyDown(KeyCode.Space) && saltosRestantes > 0)
        {
            rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse); // Aplicar fuerza hacia arriba para el salto
            saltosRestantes--; // Reducir el n�mero de saltos restantes
        }
    }

    bool EstaEnElSuelo()
    {
        RaycastHit hit;
        float distanciaAlSuelo = 0.5f;
        return Physics.Raycast(transform.position, Vector3.down, out hit, distanciaAlSuelo);
    }
}
