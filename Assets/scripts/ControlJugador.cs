using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    private void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("musica ambiental");
    }

    void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Finish"))
        {
            GestorDeAudio.instancia.ReproducirSonido("victoria");
        }
    }

}