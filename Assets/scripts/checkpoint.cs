using UnityEngine;

public class checkpoint : MonoBehaviour
{
    private bool activated = false;
    private Color originalColor;
    private Renderer platformRenderer;

    void Start()
    {
        platformRenderer = GetComponent<Renderer>();
        originalColor = platformRenderer.material.color;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !activated)
        {
            // Guardar checkpoint
            PlayerPrefs.SetFloat("CheckpointX", transform.position.x);
            PlayerPrefs.SetFloat("CheckpointY", transform.position.y);
            PlayerPrefs.SetFloat("CheckpointZ", transform.position.z);

            // Cambiar color de la plataforma a verde
            platformRenderer.material.color = Color.green;

            activated = true;

            Debug.Log("Checkpoint activado. Posici�n guardada: " + transform.position);
        }
    }

    void Update()
    {
        // Verificar si se presiona la tecla R
        if (Input.GetKeyDown(KeyCode.R))
        {
            // Llamar a la funci�n para volver al checkpoint
            ReturnToCheckpoint();
        }
    }

    void ReturnToCheckpoint()
    {
        // Obtener las coordenadas del checkpoint guardado
        float checkpointX = PlayerPrefs.GetFloat("CheckpointX");
        float checkpointY = PlayerPrefs.GetFloat("CheckpointY");
        float checkpointZ = PlayerPrefs.GetFloat("CheckpointZ");

        // Mover al jugador al checkpoint
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            player.transform.position = new Vector3(checkpointX, checkpointY, checkpointZ);

            Debug.Log("Jugador regres� al checkpoint. Nueva posici�n: " + player.transform.position);
        }
        else
        {
            Debug.LogWarning("No se encontr� el objeto del jugador. Aseg�rate de tener la etiqueta 'Player' en el objeto del jugador.");
        }
    }
}
