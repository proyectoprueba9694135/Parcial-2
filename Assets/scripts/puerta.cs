using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puerta : MonoBehaviour
{

    public GameObject door;
    public Material cambiarcolor;
    private int objectsOnPlatform = 0;
    private Material originalMaterial;

    void Start()
    {
        
        originalMaterial = GetComponent<Renderer>().material;
    }

    void Update()
    {
        if (objectsOnPlatform > 0)
        {
            
            door.SetActive(false);
            GetComponent<Renderer>().material = cambiarcolor;
        }
        else
        {
            
            door.SetActive(true);
            GetComponent<Renderer>().material = originalMaterial;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Caja"))
        {
         
            objectsOnPlatform++;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Caja"))
        {
           
            objectsOnPlatform--;
        }
    }

}
