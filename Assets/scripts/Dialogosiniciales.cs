using UnityEngine;
using TMPro;

public class Dialogosiniciales : MonoBehaviour
{
    public TMP_Text panelTexto;
    public string[] dialogos; 

    private int indiceDialogo = 0;

    void Start()
    {
        MostrarDialogoActual();
    }

    void Update()
    {
       
        if (Input.GetMouseButtonDown(1))
        {
            
            SiguienteDialogo();
        }
    }

    void MostrarDialogoActual()
    {
       
        if (indiceDialogo < dialogos.Length)
        {
           
            panelTexto.text = dialogos[indiceDialogo];
        }
        else
        {
         
            panelTexto.text = "";
            gameObject.SetActive(false);
        }
    }

    void SiguienteDialogo()
    {
        
        indiceDialogo++;

      
        MostrarDialogoActual();
    }
}

