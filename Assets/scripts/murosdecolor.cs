using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class murosdecolor : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Renderer muroRenderer = GetComponent<Renderer>();
            Renderer pelotaRenderer = collision.gameObject.GetComponent<Renderer>();


            if (muroRenderer.material.color == pelotaRenderer.material.color)
            {
               
                Debug.Log("Pelota puede pasar el muro de color " + muroRenderer.material.color);


                Destroy(gameObject);
            }
            else
            {
               
                Debug.Log("Pelota no puede pasar el muro de color " + muroRenderer.material.color);


                Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.velocity = Vector3.zero;
                }
            }
        }
    }
}
