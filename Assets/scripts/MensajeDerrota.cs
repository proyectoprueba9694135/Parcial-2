using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MensajeDerrota : MonoBehaviour
{
    public TMP_Text mensajeText;


    private bool jugadorPerdio = false;

   
    public void MostrarMensajeDerrota()
    {
        if (!jugadorPerdio)
        {
            mensajeText.text = "Perdiste. Presiona R para reiniciar.";
            jugadorPerdio = true;
        }
    }

    
    private void Update()
    {
        if (jugadorPerdio && Input.GetKeyDown(KeyCode.R))
        {
            ReiniciarJuego();
        }
    }

    private void ReiniciarJuego()
    {
      
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
}
